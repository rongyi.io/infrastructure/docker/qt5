ARG ubuntu_tag=latest

FROM ubuntu:${ubuntu_tag}

ENV DEBIAN_FRONTEND=noninteractive

# Update package list
RUN apt-get update

# Install git
RUN apt-get install -y git build-essential cmake mesa-common-dev libglu1-mesa-dev libfontconfig1 apt-utils

# Install Qt5
RUN apt-get install -y qt5-default

# Install GTest
RUN apt-get install -y libgtest-dev && \
    cd /usr/src/gtest/ && \
    cmake -DBUILD_SHARED_LIBS=ON CMakeLists.txt && \
    make && \
    find -iname '*.so' -exec cp {} /usr/lib \;
    # cp *.so /usr/lib
